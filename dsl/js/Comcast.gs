function myFunction() {

//  Initiating
  var sheet = SpreadsheetApp.getActiveSpreadsheet();
  SpreadsheetApp.setActiveSheet(sheet.getSheetByName('Copy of Central Mobile NONSUB'));
  var tab = sheet.getActiveSheet();



 //legend persistence
  var legend_height = 5;
  var legend_width = tab.getLastColumn();


  colE = tab.getRange(1,1,legend_height,legend_width).getValues();
  targetCol = tab.getRange(1, legend_width+2, legend_height, legend_width );
  targetCol.setValues(colE);
  tab.getRange(1,1,legend_height,legend_width).copyTo(targetCol, {formatOnly:true});

  var range = tab.getRange(1,1,legend_height,legend_width);
  range.clearContent();
  range.clearFormat();


// deleting priority
  for(i=1;i<tab.getLastColumn();i++) {
  if (tab.getRange(6,i).getValue() == "Priority")
  {
    tab.deleteColumn(i);
    break;
  }}

// adding columns before New Banner

  for(i=1;i<tab.getLastColumn();i++) {
    if (tab.getRange(6,i).getValue() == "New Banner")
    {
      tab.insertColumnAfter(i-1).getRange(6, i).setValue("version_name");
      tab.insertColumnAfter(i-1).getRange(6, i).setValue("VersionRotation");
      break;
    }
  }

//  bringing back legend

   colF = tab.getRange(1, legend_width , legend_height, legend_width).getValues();
   targetCol= tab.getRange(1,1,legend_height,legend_width);
    targetCol.setValues(colF);
   tab.getRange(1, legend_width , legend_height, legend_width).copyTo(targetCol, {formatOnly:true});

//   var clear_range=tab.getRange(1,legend_width,5,legend_width + 15);
//  tab.deleteColumns(legend_width + 2,legend_width)

  var range = tab.getRange(1,legend_width,legend_height,legend_width);
  range.clearContent();
  range.clearFormat();

}
